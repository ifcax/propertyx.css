#!/bin/bash
# Run following line one time to save Credential for active session
# git config --global credential.helper cache

#@echo off
TRD=$(tput setaf 1)
TNC=$'\e[0m'
TGR=$(tput setaf 2)
silent=false
while getopts b:t:e:q: flag; do
    case "${flag}" in
    b) branch=${OPTARG} ;;
    t) tag=${OPTARG} ;;
    e) _env=${OPTARG} ;;
    q) silent=true ;;
    esac
done

if [ -z $tag ]; then
    tag=""
else
    tag=" tags/$tag  -b B$tag"
fi

if [ "$branch" = "m" ]; then
    branch="master"
else
    branch="development"
fi

if [ "$_env" = "uat" ]; then
    _env="Uat"
elif [ "$_env" = "prod" ]; then
    _env="Prod"
else
    _env="Dev"
fi
abort() {
    echo "${TRD}An error occurred. Exiting..." >&2
    exit 1
}
trap 'abort' 0
set -e
echo "
$TNC Update Property UI: $branch $TNC
"
cd "$(dirname "${BASH_SOURCE[0]}")"
git fetch
git checkout $branch
git pull

cd ..
trap : 0
echo >&2 $TGR'
*********************************************
***    Property UI update Successfully    ***
*********************************************
'$TNC

